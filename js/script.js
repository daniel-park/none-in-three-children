
 /* change opacity on scroll
*/
 
$(window).scroll(function(){
    $(".top").css("opacity", 1 - $(window).scrollTop() / 250);
  });


var canFade = true;
var isHiding = false;
var isShowing = false;
var isHidden = false;

/* allows top nav to fade */


function fadez () {
if (canFade === true){
    if ((!isHiding && !isHidden) && (window.scrollY > 100 || window.pageYOffset > 100)) { 
        isHiding = true;
        console.log("hide");
        $('.navbarMainSite').animate({height:"0px", duration: 'slow'});
        $('.navbarMainSite').fadeOut({queue: false, duration: 'slow', complete: function () { isHiding = false; isHidden = true; }});

    }
    else if (!isShowing && isHidden && !(window.scrollY > 100 || window.pageYOffset > 100)) {
        isShowing = true;
        console.log("show");
        $('.navbarMainSite').animate({height:"40px"});
        $('.navbarMainSite').fadeIn({queue: false, duration: 'fast', complete: function () { isShowing = false; isHidden = false; }});
    }

    
    } /*console.log(fadez);     */ 
}

window.addEventListener("scroll", function() {fadez();});
                        





/* collapse nav back up on selection (mobile view) */
$(function hideNav(){
    var navMain = $("#navbar");
    navMain.on("click", "a", null, function () {navMain.collapse('hide');
    });
});

/* smooth scroll */
$(function smoothScroll() {
$('a[href*="#"]:not([href="#"]):not([data-no-smooth-scroll])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                canFade = false;
                    
                if (target.length /*&& $(window).width() > 768 */   ) {
                    $('html, body').animate({scrollTop: target.offset().top}, 1000, "swing", canCall);
                }
            }
        });
    });

/* needed for smooth scroll to ensure that the nav stays fades out and in when is supposed to */
function canCall(){
    canFade = true;
    fadez();
    
}

//////////////////////////////////////////////////////////////////////////ARRAY ORGANISATION SUPPORT INFORMATION

function Information (organisation, addressLine1, addressLine2, addressLine3, town, region, website, email, telNo, info)
{
    this.organisation=organisation;
    this.addressLine1=addressLine1;
    this.addressLine2=addressLine2;
    this.addressLine3=addressLine3;
    this.town=town;
    this.region=region;
    this.website=website;
    this.email=email;
    this.telNo=telNo;
    this.info=info;
}

var informationList=[];

informationList.push(new Information("The Child Protection Authority of Grenada", "National Stadium", "", "", "St. George's", "Grenada", "website here", "", "473-435-3396", "To report an incident where a child has been harmed, or is at risk of abuse, abandonment or neglect" ));
informationList.push(new Information("The Barbados Child Care Board", "Fred Edghill Building", "", "", "St. Michael","Barbados", "website here", "<a href='mailto:childcareboard@caribsurf.com'>childcareboard@caribsurf.com</a>", "246-426-2577", "To report an incident where a child has been harmed, or is at risk of abuse, abandonment or neglect" ));
informationList.push(new Information("Sweet Water Foundation", "c/o Hazel Da Breo", "Wavecrest Apartments", "Grand Anse", "St. George's", "Grenada", "<a href='http://www.sweetwaterfoundation.ca/'>www.sweetwaterfoundation.ca/</a>", "", "473-415-8000", "For on-line, confidential and anonymous conversations and counsel about how to prevent a child from being sexually abused, or for depth psychotherapy around healing" ));

////////////////////////////////////////////////////////////////////////// VARIABLES FOR INNER HTML

var returnValue = "";
feedback = document.getElementById('displayResults');



////////////////////////////////////////////////////////////////////////// LOCATION INFORMATION


function userLocation(){
  /* JUST RADIO BUTTON SELECTIONON REGION
    var userLoc; 
    var barbados=document.getElementById("Barbados");
    var grenada=document.getElementById("Grenada");

        if (barbados.checked) {
        userLoc=barbados.value 
        }else if (grenada.checked) {
        userLoc=grenada.value
        }
        return userLoc;
}
*/

    var userLoc; 
    var stmichael=document.getElementById("stmichael");
    var stgeorges=document.getElementById("stgeorges");

        if (stmichael.selected) {
        userLoc=stmichael.value 
        }else if (stgeorges.selected) {
        userLoc=stgeorges.value
        }
        return userLoc;
}
///////////////////////////////////////////////////////////////////// MAIN FUNCTION
function main(){

    userLoc=userLocation();
  
for(var i=0;i<informationList.length;i++)
{
    if(informationList[i].town===userLoc)   /*   JUST RADIO BUTTON SELECTION ON REGION if(informationList[i].region===userLoc)   */
    {
        returnValue+=("<b>The following Organisations can help you:</b><br>"+informationList[i].organisation+"<br><br>Tel:"+informationList[i].telNo+"<br><br>"+informationList[i].email+"<br><br>"+informationList[i].addressLine2+"<br><br>");
    }
        }
    feedback.innerHTML=returnValue;

}
var btn = document.getElementById("updateBtn");
/*btn.addEventListener("click",main, false);*/


/* display downdown based on radio selection */

/*window.document.getElementById("noOptions").style.display = "block";*/
/*window.document.getElementById("allOptions").style.display = "none";
function changeOptions() {
    var form = window.document.getElementById("userLocation");
    var BarbadosOptions = window.document.getElementById("BarbadosOptions");
    var GrenadaOptions = window.document.getElementById("GrenadaOptions");

    window.document.getElementById("noOptions").style.display = "none";

    if (form.Barbados.checked) {
        GrenadaOptions.style.display = "none";
        BarbadosOptions.style.display = "block";
        BarbadosOptions.selectedIndex = 0;
    } else if (form.Grenada.checked) {
        BarbadosOptions.style.display = "none";
        GrenadaOptions.style.display = "block";
        GrenadaOptions.selectedIndex = 0;
    }

}
window.document.getElementById("Barbados").onclick = changeOptions;
window.document.getElementById("Grenada").onclick = changeOptions;
*/





        /*     
             function populate(s1,s2){
                 var s1 = document.getElementById(s1);
                 var s2 = document.getElementById(s2);
                 s2.innerHTML = "";
                 if(s1.value == "Barbados"){
                     var optionArray = ["|","saintMichael|Saint Michael","saintJames|Saint James","christChurch|Christ Church","saintPeter|Saint Peter"];
                 }
                 else if(s1.value == "Grenada"){
                     var optionArray = ["|","stgeorges|St George's","grenville|Grenville","gouyave|Gouyave"];
                 }
                 for(var option in optionArray){
                     var pair = optionArray[option].split("|");
                     var newOption = document.createElement("option");
                     newOption.value = pair[0];
                     newOption.innerHTML = pair[1];
                     s2.options.add(newOption);
                 }
             }
             */
        
        
        
        
        
 /*                    
var fname, lname, country;

var returnValue = "res";
feedback = document.getElementById('displayResults');

function _(x){
    return document.getElementById(x);
}

function processPhase1(){
    fname = _("firstname").value;
    lname = _("lastname").value;
    if(fname.length > 2 && lname.length > 2){
        _("phase1").style.display = "none";
        _("phase2").style.display = "block";        
    }else{
        alert("Please complete phase 1");
    }
            
}
function processPhase2(){
    select2 = _("select2").value;
    if (select2.length > 0){
        _("phase2").style.display = "none";
        _("displayResults").style.display = "block";        
    }else{
        alert("Please complete phase 2");
    }
            
}

function res(){
    if (select2 = "stgeorges"){
         return "Information for you St Georges";
     }
    else if (select2.value==="christChurch")
         return "Information for you Christ Church";
}
*/




